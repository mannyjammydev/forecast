## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

## Running

Project does not use any exrternal/3rd party tools,  100% native....so far

1. Clone the project
2. Open the project
3. Select device (running iOS 12.x and above)
4. Run project

---

## Extras/Improvements

1. More rubust unit tests - Curretly there are some unit tests to test the networking functionality, I would add tests for the model
2. Polling GPS for user location - Currently the app uses the Shard in London to fetch the weather data. As per spec this should use the CoreLocation framework to get the current location. This left last as it could be easily mocked and I felt the networking module to be more important
3. Proper Git usage - In the real world I would use Git more robustly rather than just at the end to upload the project
4. More comments
