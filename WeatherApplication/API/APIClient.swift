//
//  APIClient.swift
//  WeatherApplication
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import Foundation

enum Either<V, E: Error> {
    case value(V)
    case error(E)
}

enum APIError: Error {
    case apiError
    case badURL
    case badResponse
    case jsonDecoder
    case unknown
}

protocol APIClient {
    var session: URLSession { get }
    func fetch<V: Codable>(with request: URLRequest, completion: @escaping(Either<V,APIError>) -> Void)
    }

extension APIClient {
    func fetch<V: Codable>(with request: URLRequest, completion: @escaping(Either<V,APIError>) -> Void) {
        let task = session.dataTask(with: request) {(data, response, error) in
            guard error == nil else {
                completion(.error(.apiError))
                return
            }
            guard let response = response as? HTTPURLResponse, 200..<300 ~= response.statusCode else {
                completion(.error(.badResponse))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                let results = try decoder.decode(V.self, from: data!)
                completion(.value(results))
            } catch (let jsonError) {
                print(jsonError)
                completion(.error(.jsonDecoder))
            }
            
        }
        task.resume()
    }
}


