//
//  Endpoint.swift
//  WeatherApplication
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import Foundation
import CoreLocation

protocol Endpoint {
    var baseUrl: String { get }
    var path: String { get }
    var queryItems: [URLQueryItem] { get }
}

extension Endpoint {
    var urlComponent: URLComponents? {
        var component = URLComponents(string: baseUrl)
        component?.path = path
        component?.queryItems = queryItems
        
        return component
    }
    
    var request: URLRequest? {
        guard let url = urlComponent?.url else { return nil }
        return URLRequest(url: url)
    }
}

enum WeatherEndpoint: Endpoint {
    
    case forcast(_ location: CLLocationCoordinate2D)
    
    var baseUrl: String {
        return "https://api.openweathermap.org"
    }
    
    var path: String {
        switch self {
        case .forcast(_):
            return "/data/2.5/forecast"
        }
    }
    
    var queryItems: [URLQueryItem] {
        
        // All requests should be using the same key/appid
        var params = [URLQueryItem(name: "appid", value: "c92dcc692369a6eea28dcb9d0356c36a")]
        
        switch self {
        case .forcast(let location):
            params.append(URLQueryItem(name: "lat", value: String(location.latitude)))
            params.append(URLQueryItem(name: "lon", value: String(location.longitude)))
        }
        
        return params
    }
}
