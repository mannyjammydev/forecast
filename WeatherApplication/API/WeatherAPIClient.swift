//
//  WeatherAPIClient.swift
//  WeatherApplication
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import Foundation

class WeatherAPIClient: APIClient {
    var session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func weather(with endpoint: WeatherEndpoint, completion: @escaping (Either<Weather, APIError>) -> Void) {
        
        guard let request = endpoint.request else { return completion(.error(.badURL)) }
        
        self.fetch(with: request) { (either: Either<Weather, APIError>) in
            switch either {
            case .value (let weather):
                completion(.value(weather))
            case .error (let error):
                completion(.error(error))
                break
            }
        }
    }
}
