//
//  Weather.swift
//  WeatherApplication
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import Foundation

struct Weather: Codable {
    
    var code: String
    var count: Int
    var list: [WeatherInformation]
    
    enum CodingKeys: String, CodingKey {
        case code = "cod"
        case count = "cnt"
        case list
    }
}

struct WeatherValues: Codable {
    
    var temperature: Double
    var temperatureMax: Double
    var temperatureMin: Double
    var pressure: Double
    var seaLevel: Double
    var groundLevel: Double
    var humidity: Double
    
    enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case temperatureMin = "temp_min"
        case temperatureMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case groundLevel = "grnd_level"
        case humidity
        
    }
}

struct WeatherInformation: Codable {
    
    var weather: [WeatherCondition]
    var main: WeatherValues
    var time: Date
    
    enum CodingKeys: String, CodingKey {
        case weather
        case main
        case time = "dt"
    }
    
}

struct WeatherCondition: Codable {
    
    var id: Int
    var main: String
    var desc: String
    var icon: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case main
        case desc = "description"
        case icon
    }
    
}
