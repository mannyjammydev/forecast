//
//  File.swift
//  WeatherApplication
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import Foundation
import CoreLocation

typealias UpdateResponse = (Bool) -> Swift.Void

class WeatherModel {
    
    private let client: WeatherAPIClient
    
    private var forcast: [WeatherInformation]
    
    init(_ client: WeatherAPIClient) {
        self.client = client
        forcast = [WeatherInformation]()
    }
    
    // Defaulting location to London
    func updateForcast(at location: CLLocationCoordinate2D, completion: @escaping UpdateResponse) {
        
        let fiveDayForcast = WeatherEndpoint.forcast(location)
        
        client.weather(with: fiveDayForcast) { (either) in
            
            switch either {
            case .value(let weather):
                self.forcast = weather.list
                completion(true)
            case .error(let error):
                print(error)
                completion(false)
            }
            
        }
        
    }
    
    // MARK: - TableView Data Source
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItemsInSection(_ section: Int) -> Int {
        return forcast.count
    }
    
    func item(at indexPath: IndexPath) -> WeatherInformation? {
        
        if indexPath.row < 0 || indexPath.row > forcast.count { return nil }
        return forcast[indexPath.row]
        
    }
    
}
