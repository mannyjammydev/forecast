//
//  WeatherTableViewController.swift
//  WeatherApplication
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherTableViewController: UITableViewController {

    private var weatherModel: WeatherModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let client = WeatherAPIClient(session: URLSession.shared)
        weatherModel = WeatherModel(client)
        
        let londonShard = CLLocationCoordinate2D(latitude: 51.5044, longitude: -0.0863)
        
        weatherModel.updateForcast(at: londonShard) { updateResult in
            
            switch updateResult {
            case true:
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case false: print("Error")
            }
            
        }
    }
    
    var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd/MMM HH:mm"
        return dateFormatter
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return weatherModel.numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherModel.numberOfItemsInSection(section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath)

        guard let weatherInfo = weatherModel.item(at: indexPath) else {
            return cell
        }
        
        cell.textLabel?.text = dateFormatter.string(from: weatherInfo.time)
        
        let weatherDesc = weatherInfo.weather.map({ $0.desc.capitalized }).joined(separator: ", ")
        let temp = String(format: "%.1f", weatherInfo.main.temperature.celsius)
        
        cell.detailTextLabel?.text = "\(weatherDesc) - Temperature \(temp) C"

        return cell
    }

}

extension Double {
    
    var kelvin: Double { return self }
    var celsius: Double { return self - 273.15 }
    var fahrenheit: Double { return (self.celsius * 1.8) + 32 }
}
