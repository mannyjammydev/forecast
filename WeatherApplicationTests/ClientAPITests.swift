//
//  ClientAPITests.swift
//  WeatherApplicationTests
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import XCTest
import CoreLocation
@testable import WeatherApplication

class ClientAPITests: XCTestCase {

    private let defaultTimeout = 15.0
    private let londonShard = CLLocationCoordinate2D(latitude: 51.5044, longitude: -0.0863)
    
    var systemUnderTest: WeatherAPIClient!
    
    override func setUp() {
        systemUnderTest =  WeatherAPIClient(session: URLSession(configuration: .ephemeral))
    }
    
    override func tearDown() {
        systemUnderTest = nil
    }
    
    func testValidAPIConnection() {
        
        let request = WeatherEndpoint.forcast(londonShard)
        let promise = expectation(description: "Valid API Connection")
        
        systemUnderTest.weather(with: request) { result in
            promise.fulfill()
        }
        
        waitForExpectations(timeout: defaultTimeout, handler: nil)
    }
    
    func testReceivedDataFromServer() {
        
        let request = WeatherEndpoint.forcast(londonShard)
        let promise = expectation(description: "Received Data from API")
        var connectionResult: Either<Weather, APIError>?
        
        systemUnderTest.weather(with: request) { result in
            connectionResult = result
            promise.fulfill()
        }
        
        waitForExpectations(timeout: defaultTimeout, handler: nil)
        
        guard let actualResult = connectionResult else { return XCTFail() }
        
        switch actualResult {
        case .value(_): break
        case .error(let error):
                XCTFail(error.localizedDescription)
        }
    }

}
