//
//  RequestBuilderTests.swift
//  WeatherApplicationTests
//
//  Created by mannyjammy on 28/02/2019.
//  Copyright © 2019 mannyjammy. All rights reserved.
//

import XCTest
import CoreLocation
@testable import WeatherApplication

class RequestBuilderTests: XCTestCase {
    
    private let expectedURL = URLComponents(string: "https://api.openweathermap.org/data/2.5/forecast?appid=c92dcc692369a6eea28dcb9d0356c36a&lat=51.5044&lon=-0.0863")
    
    var systemUnderTest: WeatherEndpoint!
    
    override func setUp() {
        systemUnderTest = WeatherEndpoint.forcast(CLLocationCoordinate2D(latitude: 51.5044, longitude: -0.0863))
    }
    
    override func tearDown() {
        systemUnderTest = nil
    }
    
    func testForcast_LondonShard_NonNilURLComponents() {
        
        // When we get the URLComps
        let result = systemUnderTest.urlComponent
        
        // Then the URLComps should NOT be nil
        XCTAssertNotNil(result)
    }
    
    func testForcast_LondonShard_NonNilURL() {
        
        // When we get the URL form the URLComps
        let result = systemUnderTest.urlComponent?.url
        
        // Then the URL should NOT be nil
        XCTAssertNotNil(result)
        
    }
    
    func testFiveDataForcast_LondonUK_ValidHost() {
        
        // When we get Post
        let sutHost = systemUnderTest.urlComponent?.host
        let expectedHost = expectedURL!.host!
        
        // Then when we comapre them
        let result = sutHost?.compare(expectedHost)
        
        // They should be the same
        XCTAssertEqual(result, ComparisonResult.orderedSame, "Invalid API Host")
        
    }
    
    func testFiveDataForcast_LondonUK_ValidPath() {
        
        // When we get the Path
        let sutPath = systemUnderTest.urlComponent?.path
        let expectedPath = expectedURL!.path
        
        // Then when we compare them
        let result = sutPath?.compare(expectedPath)
        
        // They should be the same
        XCTAssertEqual(result, ComparisonResult.orderedSame, "Invalid API Path")
        
    }
    
}
